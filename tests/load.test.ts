import load from '../src/load';

describe('loader', () => {
	it('loads dotenv file', () => {
		expect(() => {
			load({
				path: `${__dirname}/.env.example`,
				example: `${__dirname}/.env.example`,
			});
		}).not.toThrow();
	});
});
